import { Server } from 'hapi';
import { config } from '../config';
import { routes } from './routes/';

// Create a server with a host and port
const server: Server = new Server( {
  host: process.env.HOST || config.HOST, // to allow external connections from Docker container
  port: process.env.PORT || config.PORT,
  routes: {
    cors: true,
  },
} );

// Add API prefix
server.realm.modifiers.route.prefix = config.API_PATH;

// Add all the routes within the routes folder
Object.keys( routes ).forEach( route => server.route( routes[route] ) );

// Start the server
server.start()
  .then( () => console.info( `Server running at: ${server.info.uri}` ) );

process.on( 'unhandledRejection', ( err ) => {
  console.error( err );
  process.exit( 1 );
} );
