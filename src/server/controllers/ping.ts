export const pingController: Dictionary<RouteMethod> = {
  getPing: (request, f, err) =>
    err
      ? { errorCode: err.name, errorMessage: err.message }
      : { message: 'Get Pong' },
  postPing: (request, f, err) =>
    err
      ? { errorCode: err.name, errorMessage: err.message }
      : { message: 'Post Pong' },
};
