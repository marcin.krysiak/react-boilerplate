import { pingController } from './ping';

describe( 'ping controller', () => {
  const getPing: Function = pingController.getPing;
  it( 'handle error response with error message', () =>
    expect( getPing( null, null, { name: 'bad error', message: 'some very bad error' } ) )
      .toEqual( { errorMessage: 'some very bad error', errorCode: 'bad error' } ),
  );

  it( 'handle error response with error message', () =>
    expect( getPing() ).toEqual( { message: 'Get Pong' } ),
  );

  const postPing: Function = pingController.postPing;
  it( 'handle error response with error message', () =>
    expect( postPing( null, null, { name: 'bad error', message: 'some very bad error' } ) )
      .toEqual( { errorMessage: 'some very bad error', errorCode: 'bad error' } ),
  );

  it( 'handle error response with error message', () =>
    expect( postPing() ).toEqual( { message: 'Post Pong' } ),
  );
} );
