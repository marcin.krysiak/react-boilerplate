import { prefix } from '../../utils/prefixer';
import { styles as sharedStyles } from '../../global.styles';

export const styles = prefix( {
  container : {
    display: 'flex',
    width: '100%',
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: sharedStyles.colors.black,
    color: 'white',
  } as React.CSSProperties,
  text: {
    padding: sharedStyles.gutter.normal,
  },
  image: {
    width: '50%',
    height: 'auto',
    padding: sharedStyles.gutter.normal,
  },
} );
