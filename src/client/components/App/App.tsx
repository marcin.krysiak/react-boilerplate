import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { State } from '../../redux/state';
import { getHelloMessageDispatcher } from '../../redux/branches/getHello';
import { helloMessageSelector } from '../../redux/selectors/getHello';
import { withRouter, RouteComponentProps } from 'react-router';
import { styles } from './app.styles';

const someIcon = require('../../assets/some-icon.png');

export interface OwnProps {
  readonly helloMessage: string;
  readonly getHelloMessage: () => void;
}

type Props = OwnProps & RouteComponentProps<null>;

export class App extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
    this.buttonHandler = this.buttonHandler.bind(this);
  }

  componentDidMount() {
    this.props.getHelloMessage();
  }

  buttonHandler(e: React.MouseEvent<HTMLElement>) {
    console.log('button handler');
    this.props.history.push('/dialog');
  }

  render() {
    return (
      <div style={styles.container}>
        <span style={styles.text}>Hello world: {this.props.helloMessage}</span>
        <img style={styles.image} src={someIcon} />
        <Button variant="raised" color="secondary" onClick={this.buttonHandler}>
          test material ui button - Open alert dialog
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state: State, props: Props) => ({
  helloMessage: helloMessageSelector(state),
});

const mapDispatchToProps = (dispatch: Function) => ({
  getHelloMessage: () => dispatch(getHelloMessageDispatcher()),
});

export default compose(
  withRouter,
  connect<{}, {}, Props>(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(App);
