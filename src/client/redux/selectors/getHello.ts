import { State } from '../state';

export const helloMessageSelector = ( state: State ) => state.helloMessage;
