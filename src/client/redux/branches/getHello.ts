import { createAction } from 'redux-actions';
import * as actionTypes from '../actions';
import { DEFAULT_STATE, State } from '../state';
import { Dispatch } from 'redux';
import { API_URL } from '../../index';

interface GetHelloMessageActionPayload {
  message: string;
}

// Action creators
export const getHelloMessageAction = createAction(
  actionTypes.HELLO_GET,
  (payload: GetHelloMessageActionPayload): GetHelloMessageActionPayload =>
    payload,
);

// // Dispatcher
export const getHelloMessageDispatcher = () => (
  dispatch: Dispatch<PayloadAction<GetHelloMessageActionPayload>>,
) => {
  fetch(`${API_URL}hello`, { method: 'GET' })
    .then(response => response.json())
    .then(jsonResponse => dispatch(getHelloMessageAction(jsonResponse)))
    .catch(error => {
      console.error('error', error); // implement some global or local error handling here
    });
};

// Reducer
export const helloMessage = (
  state: State = DEFAULT_STATE,
  action: PayloadAction<GetHelloMessageActionPayload>,
): string => {
  switch (action.type) {
    case actionTypes.HELLO_GET:
      return (action.payload && action.payload.message) || '';
    default:
      return '';
  }
};
