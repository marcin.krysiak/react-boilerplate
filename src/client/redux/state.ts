import { RouterState } from 'react-router-redux';

export interface State {
  helloMessage: string;
  router: RouterState;
}

export const DEFAULT_STATE: State = {
  helloMessage: '',
  router: {
    location: null,
  },
};
